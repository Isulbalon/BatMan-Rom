#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY:37830656:9261f76e6d0c936d4f4a14265fc91fc0192e1f7a; then
  applypatch EMMC:/dev/block/platform/11120000.ufs/by-name/BOOT:33265664:fc1e705773a7dc0dbab54f6cc7ef4db5625a170c EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY 9261f76e6d0c936d4f4a14265fc91fc0192e1f7a 37830656 fc1e705773a7dc0dbab54f6cc7ef4db5625a170c:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
