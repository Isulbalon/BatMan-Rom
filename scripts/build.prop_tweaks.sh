#!/sbin/sh
# Written by Tkkg1994

aromabuildprop=/tmp/aroma/buildtweaks.prop
buildprop=/system/build.prop

getprop ro.boot.bootloader >> /tmp/BLmodel

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system
else
	mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system
fi

if grep -q item.1.24=1 /tmp/aroma/samsung.prop; then
	echo "Enable knox/tima since knox has been enabled"
	sed -i -- 's/ro.config.knox=v00/ro.config.knox=v30/g' $buildprop
fi

if grep -q finger=1 $aromabuildprop; then
	echo "# Fingerprint Tweak" >> $buildprop
	echo "fingerprint.unlock=1" >> $buildprop
fi

if grep -q user=1 $aromabuildprop; then
	echo "# Multiuser Tweaks" >> $buildprop
	echo "fw.max_users=30" >> $buildprop
	echo "fw.show_multiuserui=1" >> $buildprop
	echo "fw.show_hidden_users=1" >> $buildprop
	echo "fw.power_user_switcher=1" >> $buildprop
fi

if grep -q fix=1 $aromabuildprop; then
	echo "# Screen mirror fix" >> $buildprop
	echo "wlan.wfd.hdcp=disable" >> $buildprop
fi

if grep -q G930 /tmp/BLmodel; then
	echo "# hero Model" >> $buildprop
	echo "batman.id=hero" >> $buildprop
else if grep -q G935 /tmp/BLmodel; then
	echo "# hero2 Model" >> $buildprop
	echo "batman.id=hero2" >> $buildprop
else if grep -q G950 /tmp/BLmodel; then
	echo "# dream Model" >> $buildprop
	echo "batman.id=dream" >> $buildprop
else if grep -q G955 /tmp/BLmodel; then
	echo "# dream2 Model" >> $buildprop
	echo "batman.id=dream2" >> $buildprop
else if grep -q N950 /tmp/BLmodel; then
	echo "# great Model" >> $buildprop
	echo "batman.id=great" >> $buildprop
else
	echo "Unsupported model" >> $buildprop
fi
fi
fi
fi
fi

exit 10
