#!/sbin/sh
# Written by Tkkg1994

getprop ro.boot.bootloader >> /tmp/BLmodel
ACTUAL_CSC=`cat /efs/imei/mps_code.dat`
ACTUAL_OMC=`cat /efs/imei/omcnw_code.dat`
SALES_CODE=`cat /system/omc/sales_code.dat`
sed -i -- "s/CSC=//g" /tmp/aroma/csc.prop
NEW_CSC=`cat /tmp/aroma/csc.prop`
aromabuildprop=/tmp/aroma/buildtweaks.prop
buildprop=/system/build.prop

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system
else
	mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system
fi

# Change CSC to right model on the s8
if grep -q G955 /tmp/BLmodel; then
	sed -i -- 's/G950/G955/g' /system/etc/security/audit_filter_table
	sed -i -- 's/G950/G955/g' /system/omc/CSCVersion.txt
	sed -i -- 's/G950/G955/g' /system/omc/SW_Configuration.xml
	sed -i -- 's/G950/G955/g' /system/info.extra
	find /system -type f -name 'omc*' | xargs sed -i 's/SM-G950F/SM-G955F/g'
else if grep -q G950 /tmp/BLmodel; then
	sed -i -- 's/G955/G950/g' /system/etc/security/audit_filter_table
	sed -i -- 's/G955/G950/g' /system/omc/CSCVersion.txt
	sed -i -- 's/G955/G950/g' /system/omc/SW_Configuration.xml
	sed -i -- 's/G955/G950/g' /system/info.extra
	find /system -type f -name 'omc*' | xargs sed -i 's/SM-G955F/SM-G950F/g'
else
	echo "Not a S8, checking for S7"
fi
fi

# Change CSC to right model on the s7
if grep -q s8prop=1 $aromabuildprop; then
	if grep -q G935 /tmp/BLmodel; then
		sed -i -- 's/G950/G955/g' /system/etc/security/audit_filter_table
		sed -i -- 's/G950/G955/g' /system/omc/CSCVersion.txt
		sed -i -- 's/G950/G955/g' /system/omc/SW_Configuration.xml
		sed -i -- 's/G950/G955/g' /system/info.extra
		find /system -type f -name 'omc*' | xargs sed -i 's/SM-G950F/SM-G955F/g'
	else if grep -q G930 /tmp/BLmodel; then
		sed -i -- 's/G955/G950/g' /system/etc/security/audit_filter_table
		sed -i -- 's/G955/G950/g' /system/omc/CSCVersion.txt
		sed -i -- 's/G955/G950/g' /system/omc/SW_Configuration.xml
		sed -i -- 's/G955/G950/g' /system/info.extra
		find /system -type f -name 'omc*' | xargs sed -i 's/SM-G955F/SM-G950F/g'
	else
		echo "Not a supported model, keep csc config default!"
	fi
	fi
else
	if grep -q G935 /tmp/BLmodel; then
		sed -i -- 's/G955/G935/g' /system/etc/security/audit_filter_table
		sed -i -- 's/G955/G935/g' /system/omc/CSCVersion.txt
		sed -i -- 's/G955/G935/g' /system/omc/SW_Configuration.xml
		sed -i -- 's/G955/G935/g' /system/info.extra
		find /system -type f -name 'omc*' | xargs sed -i 's/SM-G955F/SM-G935F/g'
	else if grep -q G930 /tmp/BLmodel; then
		sed -i -- 's/G955/G930/g' /system/etc/security/audit_filter_table
		sed -i -- 's/G955/G930/g' /system/omc/CSCVersion.txt
		sed -i -- 's/G955/G930/g' /system/omc/SW_Configuration.xml
		sed -i -- 's/G955/G930/g' /system/info.extra
		find /system -type f -name 'omc*' | xargs sed -i 's/SM-G955F/SM-G930F/g'
	else
		echo "Not a supported model, keep csc config default!"

	fi
	fi
fi

# Change build.prop to right model on the s8
if grep -q G955 /tmp/BLmodel; then
	if grep -q G955F /tmp/BLmodel; then
		echo "Already a G955F model, nothing to change"
	fi
	if grep -q G955N /tmp/BLmodel; then
		sed -i -- 's/G955F/G955N/g' $buildprop
		echo "Changed to G955N"
	fi
else if grep -q G950 /tmp/BLmodel; then
	sed -i -- 's/dream2lte/dreamlte/g' $buildprop
	sed -i -- 's/ro.sf.lcd_density=420/ro.sf.lcd_density=480/g' $buildprop
	sed -i -- 's/ro.sf.init.lcd_density=560/ro.sf.init.lcd_density=640/g' $buildprop
	if grep -q G950F /tmp/BLmodel; then
		sed -i -- 's/G955F/G950F/g' $buildprop
		echo "Changed to G950F"
	fi
	if grep -q G950N /tmp/BLmodel; then
		sed -i -- 's/G955F/G950N/g' $buildprop
		echo "Changed to G950N"
	fi
else
	echo "Not a S8, checking for S7"
fi
fi

# Change build.prop to right model on the s7
if grep -q s8prop=1 $aromabuildprop; then
	if grep -q G935 /tmp/BLmodel; then
		if grep -q G935F /tmp/BLmodel; then
			echo "Already a G955F model"
		fi
		if grep -q G935K /tmp/BLmodel; then
		sed -i -- 's/G955F/G955N/g' $buildprop
			echo "Changed to G955N"
		fi
		if grep -q G935L /tmp/BLmodel; then
		sed -i -- 's/G955F/G955N/g' $buildprop
			echo "Changed to G955N"
		fi
		if grep -q G935S /tmp/BLmodel; then
		sed -i -- 's/G955F/G955N/g' $buildprop
			echo "Changed to G955N"
		fi
		if grep -q G935W8 /tmp/BLmodel; then
			echo "Keep G955F as the W model is now snapdragon"
		fi
	else if grep -q G930 /tmp/BLmodel; then
		sed -i -- 's/dream2lte/dreamlte/g' $buildprop
		sed -i -- 's/ro.sf.lcd_density=420/ro.sf.lcd_density=480/g' $buildprop
		sed -i -- 's/ro.sf.init.lcd_density=560/ro.sf.init.lcd_density=640/g' $buildprop
		if grep -q G930F /tmp/BLmodel; then
		sed -i -- 's/G955F/G950F/g' $buildprop
			echo "Changed to G950F"
		fi
		if grep -q G930K /tmp/BLmodel; then
		sed -i -- 's/G955F/G950N/g' $buildprop
			echo "Changed to G950N"
		fi
		if grep -q G930L /tmp/BLmodel; then
		sed -i -- 's/G955F/G950N/g' $buildprop
			echo "Changed to G950N"
		fi
		if grep -q G930S /tmp/BLmodel; then
		sed -i -- 's/G955F/G950N/g' $buildprop
			echo "Changed to G950N"
		fi
		if grep -q G935W8 /tmp/BLmodel; then
		sed -i -- 's/G955F/G950F/g' $buildprop
			echo "Keep G950F as the W model is now snapdragon"
		fi
	else
		echo "Not a supported model, keep build.prop default!"
	fi
	fi
else
	if grep -q G935 /tmp/BLmodel; then
		sed -i -- 's/ro.build.flavor=dream2ltexx-user/ro.build.flavor=hero2ltexx-user/g' $buildprop
		sed -i -- 's/ro.product.name=dream2ltexx/ro.product.name=hero2ltexx/g' $buildprop
		sed -i -- 's/ro.product.device=dream2lte/ro.product.device=hero2lte/g' $buildprop
		if grep -q G935F /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G955F/ro.product.model=SM-G935F/g' $buildprop
			echo "Changed to G935F"
		fi
		if grep -q G935K /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G955F/ro.product.model=SM-G935K/g' $buildprop
			echo "Changed to G935K"
		fi
		if grep -q G935L /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G955F/ro.product.model=SM-G935L/g' $buildprop
			echo "Changed to G935L"
		fi
		if grep -q G935S /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G955F/ro.product.model=SM-G935S/g' $buildprop
			echo "Changed to G935S"
		fi
		if grep -q G935W8 /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G955F/ro.product.model=SM-G935W8/g' $buildprop
			echo "Changed to G935W8"
		fi
	else if grep -q G930 /tmp/BLmodel; then
		sed -i -- 's/dream2lte/dreamlte/g' $buildprop
		sed -i -- 's/G955F/G950F/g' $buildprop
		sed -i -- 's/ro.sf.lcd_density=420/ro.sf.lcd_density=480/g' $buildprop
		sed -i -- 's/ro.sf.init.lcd_density=560/ro.sf.init.lcd_density=640/g' $buildprop
		sed -i -- 's/ro.build.flavor=dreamltexx-user/ro.build.flavor=heroltexx-user/g' $buildprop
		sed -i -- 's/ro.product.name=dreamltexx/ro.product.name=heroltexx/g' $buildprop
		sed -i -- 's/ro.product.device=dreamlte/ro.product.device=herolte/g' $buildprop
		if grep -q G930F /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G950F/ro.product.model=SM-G930F/g' $buildprop
			echo "Changed to G930F"
		fi
		if grep -q G930K /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G950F/ro.product.model=SM-G930K/g' $buildprop
			echo "Changed to G930K"
		fi
		if grep -q G930L /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G950F/ro.product.model=SM-G930L/g' $buildprop
			echo "Changed to G930L"
		fi
		if grep -q G930S /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G950F/ro.product.model=SM-G930S/g' $buildprop
			echo "Changed to G930S"
		fi
		if grep -q G930W8 /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-G950F/ro.product.model=SM-G930W8/g' $buildprop
			echo "Changed to G930W8"
		fi
	else
		echo "Not a supported model, keep build.prop default!"
	fi
	fi
fi

if grep -q G93 /tmp/BLmodel; then
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	rm -r /efs/imei/omcnw_code.dat
	cp -r /efs/imei/mps_code.dat /efs/imei/omcnw_code.dat
	chmod 0664 /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat
else
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat
fi

if grep -q G93 /tmp/BLmodel; then
	sed -i -- 's/8895/8890/g' $buildprop
	sed -i -- 's/abox/arizona/g' $buildprop
	sed -i -- 's/ro.security.vaultkeeper.feature=1/ro.security.vaultkeeper.feature=0/g' $buildprop
	sed -i -- 's/cortex-a53/cortex-a15/g' $buildprop
	sed -i -- 's/exynos-m2/exynos-m1/g' $buildprop
	sed -i -- 's/16/0/g' /system/etc/floating_feature.xml
	echo "ro.exynos.dss=1" >>  $buildprop
fi

exit 10
